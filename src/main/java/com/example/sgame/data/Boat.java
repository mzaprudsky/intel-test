package com.example.sgame.data;

import lombok.Data;

import java.util.List;

/**
 * Created by michaelz on 11/10/2021.
 */
@Data
public class Boat {
    private final List<Coordinate> coordinates;
    private final BoatType boatType;

    public Boat(List<Coordinate> coordinates, BoatType boatType) {
        this.coordinates = coordinates;
        this.boatType = boatType;
    }
}
