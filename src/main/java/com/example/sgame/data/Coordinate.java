package com.example.sgame.data;

import lombok.Data;

/**
 * Created by michaelz on 11/10/2021.
 */
@Data
public class Coordinate {
    private final int x;
    private final int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
