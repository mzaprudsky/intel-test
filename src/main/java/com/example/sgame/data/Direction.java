package com.example.sgame.data;

/**
 * Created by michaelz on 11/10/2021.
 */
public enum Direction {
    HORIZONTAL, VERTICAL
}
