package com.example.sgame.data;

/**
 * Created by michaelz on 11/10/2021.
 */
public enum BoatType {
    SINGLE, DOUBLE, TRIPLE, QUADRO
}
