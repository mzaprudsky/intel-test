package com.example.sgame.data;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by michaelz on 11/10/2021.
 */
@Getter
public class Fleet {
    private List<Boat> horizontalList = new ArrayList<>();
    private List<Boat> verticalList = new ArrayList<>();

    public void addBoat(Boat boat, Direction direction) {
        switch (direction) {
            case HORIZONTAL:
                horizontalList.add(boat);
                break;
            case VERTICAL:
                verticalList.add(boat);
                break;
        }
    }

    public Boat getAvailableBoat() {
        //local replica of external rand function
        double rand = Math.random();
        Boat boat;
        Direction direction = (rand * 10) % 2 == 0 ? Direction.HORIZONTAL : Direction.VERTICAL;
        // If the rand generated direction is not available - set boat from other direction
        if(direction == Direction.HORIZONTAL && !horizontalList.isEmpty() || verticalList.isEmpty()) {
            int availableSize = horizontalList.size();
            boat = horizontalList.get((int) (rand * availableSize));
        } else {
            int availableSize = verticalList.size();
            boat = verticalList.get((int) (rand * availableSize));
        }

        return boat;
    }

    public void removeNonValidOptions(Boat boat) {
        this.horizontalList = horizontalList.stream().filter(hBoat -> {
            return hBoat.getCoordinates().stream().noneMatch(coordinate -> {
                return boat.getCoordinates().contains(coordinate);
            });
        }).collect(Collectors.toList());

        this.verticalList = verticalList.stream().filter(vBoat -> {
            return vBoat.getCoordinates().stream().noneMatch(coordinate -> {
                return boat.getCoordinates().contains(coordinate);
            });
        }).collect(Collectors.toList());
    }
}
