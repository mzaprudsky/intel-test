package com.example.sgame;

import com.example.sgame.data.*;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by michaelz on 11/10/2021.
 */
public class SubmarineGame {
    private final int boardSize = 8;
    public static final int QUADRO_BOATS_NUM = 1;
    public static final int TRIPLE_BOATS_NUM = 2;
    public static final int DOUBLE_BOATS_NUM = 3;
    public static final int SINGLE_BOATS_NUM = 4;

    private final Fleet quadroBoats = new Fleet();
    private final Fleet tripleBoats = new Fleet();
    private final Fleet doubleBoats = new Fleet();
    private final Fleet singleBoats = new Fleet();

    @Getter
    private final List<Boat> boatsOnBoard = new ArrayList<>();

    public SubmarineGame() {
        prepareBoard();
        initBoard();
    }


    private void prepareBoard() {
        for(int i = 0; i < boardSize; i++) {
            for(int j = 0; j < boardSize; j++) {
                Coordinate coordinate = new Coordinate(i, j);
                addQuadroBoat(coordinate);
                addTripleBoat(coordinate);
                addDoubleBoat(coordinate);
                addSingleBoat(coordinate);
            }
        }
    }

    private void addQuadroBoat(Coordinate coordinate) {
        if(coordinate.getX() < boardSize - 3) {
            List<Coordinate> coordinates = createXCoordinates(coordinate, 4);
            quadroBoats.addBoat(new Boat(coordinates, BoatType.QUADRO), Direction.HORIZONTAL);
        }
        if(coordinate.getY() < boardSize - 3) {
            List<Coordinate> coordinates = createYCoordinates(coordinate, 4);
            quadroBoats.addBoat(new Boat(coordinates, BoatType.QUADRO), Direction.VERTICAL);
        }
    }

    private void addTripleBoat(Coordinate coordinate) {
        if(coordinate.getX() < boardSize - 2) {
            List<Coordinate> coordinates = createXCoordinates(coordinate, 3);
            tripleBoats.addBoat(new Boat(coordinates, BoatType.TRIPLE), Direction.HORIZONTAL);
        }
        if(coordinate.getY() < boardSize - 2) {
            List<Coordinate> coordinates = createYCoordinates(coordinate, 3);
            tripleBoats.addBoat(new Boat(coordinates, BoatType.TRIPLE), Direction.VERTICAL);
        }
    }

    private void addDoubleBoat(Coordinate coordinate) {
        if(coordinate.getX() < boardSize - 1) {
            List<Coordinate> coordinates = createXCoordinates(coordinate, 2);
            doubleBoats.addBoat(new Boat(coordinates, BoatType.DOUBLE), Direction.HORIZONTAL);
        }
        if(coordinate.getY() < boardSize - 1) {
            List<Coordinate> coordinates = createYCoordinates(coordinate, 2);
            doubleBoats.addBoat(new Boat(coordinates, BoatType.DOUBLE), Direction.VERTICAL);
        }
    }

    private void addSingleBoat(Coordinate coordinate) {
        singleBoats.addBoat(new Boat(Collections.singletonList(coordinate), BoatType.SINGLE), Direction.HORIZONTAL);
    }

    private void initBoard() {
        setQuadroBoats();
        setTripleBoats();
        setDoubleBoats();
        setSingleBoats();
    }

    private void setQuadroBoats() {
        for(int i = 0; i < QUADRO_BOATS_NUM; i++) {
            Boat boat = quadroBoats.getAvailableBoat();
            setBoatOnBoards(boat);
        }
    }
    private void setTripleBoats() {
        for(int i = 0; i < TRIPLE_BOATS_NUM; i++) {
            Boat boat = tripleBoats.getAvailableBoat();
            setBoatOnBoards(boat);
        }
    }
    private void setDoubleBoats() {
        for(int i = 0; i < DOUBLE_BOATS_NUM; i++) {
            Boat boat = doubleBoats.getAvailableBoat();
            setBoatOnBoards(boat);
        }
    }
    private void setSingleBoats() {
        for(int i = 0; i < SINGLE_BOATS_NUM; i++) {
            Boat boat = singleBoats.getAvailableBoat();
            setBoatOnBoards(boat);
        }
    }

    private void setBoatOnBoards(Boat boat) {
        boatsOnBoard.add(boat);
        quadroBoats.removeNonValidOptions(boat);
        tripleBoats.removeNonValidOptions(boat);
        doubleBoats.removeNonValidOptions(boat);
        singleBoats.removeNonValidOptions(boat);
    }

    private List<Coordinate> createXCoordinates(Coordinate coordinate, int boatSize) {
        List<Coordinate> coordinates = Stream.of(coordinate).collect(Collectors.toList());
        IntStream.range(coordinate.getX() + 1, coordinate.getX() + boatSize).forEach(x ->
                coordinates.add(new Coordinate(x, coordinate.getY()))
        );
        return coordinates;
    }
    private List<Coordinate> createYCoordinates(Coordinate coordinate, int boatSize) {
        List<Coordinate> coordinates = Stream.of(coordinate).collect(Collectors.toList());
        IntStream.range(coordinate.getY() + 1, coordinate.getY() + boatSize).forEach(y ->
                coordinates.add(new Coordinate(coordinate.getX(), y))
        );
        return coordinates;
    }
}
