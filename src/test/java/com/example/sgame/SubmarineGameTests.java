package com.example.sgame;

import com.example.sgame.data.Boat;
import com.example.sgame.data.BoatType;
import com.example.sgame.data.Coordinate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class SubmarineGameTests {


	@Test
	void submarineTest() {
		SubmarineGame submarineGame = new SubmarineGame();
		List<Boat> boatsOnBoard = submarineGame.getBoatsOnBoard();

		long quadroBoats = boatsOnBoard.stream().filter(boat -> boat.getBoatType() == BoatType.QUADRO).count();
		Assertions.assertEquals(SubmarineGame.QUADRO_BOATS_NUM, quadroBoats);

		long tripleBoats = boatsOnBoard.stream().filter(boat -> boat.getBoatType() == BoatType.TRIPLE).count();
		Assertions.assertEquals(SubmarineGame.TRIPLE_BOATS_NUM, tripleBoats);

		long doubleBoats = boatsOnBoard.stream().filter(boat -> boat.getBoatType() == BoatType.DOUBLE).count();
		Assertions.assertEquals(SubmarineGame.DOUBLE_BOATS_NUM, doubleBoats);

		long singleBoats = boatsOnBoard.stream().filter(boat -> boat.getBoatType() == BoatType.SINGLE).count();
		Assertions.assertEquals(SubmarineGame.SINGLE_BOATS_NUM, singleBoats);

		// Check that same coordinate doesn't appear in multiple boats
		long numOfCoordinates = boatsOnBoard.stream().mapToLong(boat -> boat.getCoordinates().size()).sum();
		Set<Coordinate> coordinateSet = boatsOnBoard.stream().flatMap(boat -> boat.getCoordinates().stream()).collect(Collectors.toSet());
		Assertions.assertEquals(coordinateSet.size(), numOfCoordinates);
	}

}
